import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import '@/assets/css/design.css'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueCookies from "vue-cookies";
import VueMeta from "vue-meta";

Vue.config.productionTip = false

Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueCookies)
Vue.use(VueMeta);

new Vue({
  render: h => h(App),
}).$mount('#app')
