import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueCookies from "vue-cookies";
import VueAxios from "vue-axios";
import axios from "axios";
import VueFilterDateFormat from "vue-filter-date-format";
import VModal from "vue-js-modal";
import SocialSharing from "vue-social-sharing";
import VueMeta from 'vue-meta';
// import VueGtag from "vue-gtag";

Vue.config.productionTip = false

Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueCookies)
Vue.use(VueAxios, axios)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueFilterDateFormat, {
  dayOfWeekNames: [
    'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Saturday'
  ],
  dayOfWeekNamesShort: [
    'Su', 'Mo', 'Tu', 'We', 'Tr', 'Fr', 'Sa'
  ],
  monthNames: [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ],
  monthNamesShort: [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ]
});
Vue.use(SocialSharing);
Vue.use(VueMeta);
// Vue.use(VueGtag, {
//   config: { id: "UA-169935056-1",
//     params: {
//       send_page_view: false
//     }}
// });

new Vue({
  render: h => h(App),
}).$mount('#app')
