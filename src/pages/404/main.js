import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VModal from 'vue-js-modal'
import VueCookies from "vue-cookies";
import VueSession from "vue-session";
import VueMeta from "vue-meta";
import VueGtag from "vue-gtag";

Vue.use(VueAxios, axios)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueCookies)
Vue.use(VueSession, {persist: true})
Vue.use(VueMeta);
Vue.use(VueGtag, {
  config: { id: "UA-169935056-1",
    params: {
      send_page_view: false
    }}
});

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
