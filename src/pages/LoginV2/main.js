import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VModal from 'vue-js-modal'
import VueSession from 'vue-session'
import VueCrypt from 'vue-crypt'
import VueCookies from "vue-cookies";
import VueMeta from "vue-meta";
// import VueGtag from "vue-gtag";

Vue.config.productionTip = false

Vue.use(VueAxios, axios)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueCookies)
Vue.use(VueMeta);
// Vue.use(VueGtag, {
//   config: { id: "UA-169935056-1",
//     params: {
//       send_page_view: false
//     }}
// });

new Vue({
  render: h => h(App),
}).$mount('#app')
