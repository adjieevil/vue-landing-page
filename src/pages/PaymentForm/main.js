import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import VModal from 'vue-js-modal'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueCookies from "vue-cookies";
import VueAxios from "vue-axios";
import axios from "axios";
import VueMeta from "vue-meta";

Vue.config.productionTip = false
Vue.use(VModal, { componentName: "modal" })

Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueCookies)
Vue.use(VueAxios, axios)
Vue.use(VueMeta);

new Vue({
  render: h => h(App),
}).$mount('#app')
