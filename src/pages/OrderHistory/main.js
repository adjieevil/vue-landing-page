import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueCookies from "vue-cookies";
import VModal from "vue-js-modal";
import VueAxios from "vue-axios";
import axios from "axios";
import '../../../node_modules/vue-ads-pagination/dist/vue-ads-pagination.css'
import VueFilterDateFormat from "vue-filter-date-format";
import VueMeta from "vue-meta";

Vue.config.productionTip = false

Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueCookies)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueAxios, axios)
Vue.use(VueFilterDateFormat, {
  dayOfWeekNames: [
    'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
    'Friday', 'Saturday'
  ],
  dayOfWeekNamesShort: [
    'Su', 'Mo', 'Tu', 'We', 'Tr', 'Fr', 'Sa'
  ],
  monthNames: [
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
  ],
  monthNamesShort: [
    'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
  ]
});
Vue.use(VueMeta);

new Vue({
  render: h => h(App),
}).$mount('#app')
