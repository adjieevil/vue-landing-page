import Vue from 'vue'
import vueSmoothScroll from 'vue2-smooth-scroll'
import App from './App.vue'
import '@/assets/css/tailwind.css'
import AOS from 'aos'
import 'aos/dist/aos.css'
import VModal from 'vue-js-modal'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueAxios from "vue-axios";
import axios from "axios";
import VueCookies from "vue-cookies";
import VueMeta from "vue-meta";
// import VueGtag from "vue-gtag";

Vue.config.productionTip = false

Vue.use(vueSmoothScroll)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueAxios, axios)
Vue.use(VueCookies)
Vue.use(VueMeta);
// Vue.use(VueGtag, {
//   config: { id: "UA-169935056-1",
//     params: {
//       send_page_view: false
//     }}
// });

new Vue({
  render: h => h(App),
  created() {
    AOS.init();
  }
}).$mount('#app')
