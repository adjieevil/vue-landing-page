import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import '@/assets/css/product_oca.css'
import AOS from 'aos'
import 'aos/dist/aos.css'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueCookies from "vue-cookies";
import VueAxios from "vue-axios";
import axios from "axios";
import VModal from "vue-js-modal";
import VueMeta from "vue-meta";
// import VueGtag from "vue-gtag";
Vue.use(VueCrypt)

Vue.config.productionTip = false

Vue.use(VueSession, {persist: true})
Vue.use(VueCookies)
Vue.use(VueAxios, axios)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueMeta);
// Vue.use(VueGtag, {
//   config: { id: "UA-169935056-1",
//     params: {
//       send_page_view: false
//     }}
// });

new Vue({
  render: h => h(App),
  created() {
    AOS.init();
  }
}).$mount('#app')
