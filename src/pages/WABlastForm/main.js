import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import VueSession from "vue-session";
import VueCrypt from "vue-crypt";
import VueCookies from "vue-cookies";
import VModal from "vue-js-modal";
import VueMeta from "vue-meta";

Vue.config.productionTip = false
Vue.use(VueCrypt)
Vue.use(VueSession, {persist: true})
Vue.use(VueCookies)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueMeta);

new Vue({
  render: h => h(App),
}).$mount('#app')
