import Vue from 'vue'
import App from "./App";
import '@/assets/css/tailwind.css'
import VueSession from 'vue-session'
import VueCrypt from "vue-crypt";
import VueAxios from "vue-axios";
import axios from "axios";
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import '@mdi/font/css/materialdesignicons.css'
import VModal from "vue-js-modal";
import VueCookies from "vue-cookies";
import VueMeta from "vue-meta";
// import VueGtag from "vue-gtag";

Vue.config.productionTip = false

Vue.use(VueSession, {persist: true})
Vue.use(VueCrypt)
Vue.use(VueAxios, axios)
Vue.use(Buefy)
Vue.use(VModal, { componentName: "modal" })
Vue.use(VueCookies)
Vue.use(VueMeta);
// Vue.use(VueGtag, {
//   config: { id: "UA-168638569-1",
//     params: {
//       send_page_view: false
//     }}
// });
//Vue.use(Input)

new Vue({
  render: h => h(App),
}).$mount('#app')
