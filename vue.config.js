module.exports = {
    pages: {
        home: {
            // entry for the page
            entry: 'src/pages/Home/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'home']
        },
        loginV2: {
            // entry for the page
            entry: 'src/pages/LoginV2/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'loginV2']
        },
        registerV2: {
            // entry for the page
            entry: 'src/pages/RegisterV2/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'registerV2']
        },
        registerV3: {
            // entry for the page
            entry: 'src/pages/RegisterV3/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'registerV3']
        },
        index: {
            // entry for the page
            entry: 'src/pages/HomeV2/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'index']
        },
        testing: {
            // entry for the page
            entry: 'src/pages/testing/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'testing']
        },
        'product/blast': {
            // entry for the page
            entry: 'src/pages/Products/OcaBlast/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'product/blast']
        },
        'product/interaction': {
            // entry for the page
            entry: 'src/pages/Products/OcaInteraction/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'product/interaction']
        },
        'product/profiling': {
            // entry for the page
            entry: 'src/pages/Products/OcaProfiling/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'product/profiling']
        },
        'solutions/umkm': {
            // entry for the page
            entry: 'src/pages/Solutions/Umkm/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'UMKM - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'solutions/umkm']
        },
        'solutions/enterprise': {
            // entry for the page
            entry: 'src/pages/Solutions/Enterprise/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Enterprise - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'solutions/enterprise']
        },
        'solutions/startup': {
            // entry for the page
            entry: 'src/pages/Solutions/Startup/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Startup - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'solutions/startup']
        },
        'about-us/tentang-oca': {
            // entry for the page
            entry: 'src/pages/AboutUs/TentangOca/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Startup - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'about-us/tentang-oca']
        },
        'about-us/blog': {
            // entry for the page
            entry: 'src/pages/AboutUs/Blog/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Blog - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'about-us/blog']
        },
        // pricing: {
        //     // entry for the page
        //     entry: 'src/pages/Pricing/main.js',
        //     // the source template
        //     template: 'public/index.html',
        //     // output as dist/index.html
        //     // when using title option,
        //     // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
        //     title: 'Pricing - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
        //     // chunks to include on this page, by default includes
        //     // extracted common chunks and vendor chunks.
        //     chunks: ['chunk-vendors', 'chunk-common', 'pricing']
        // },
        pricing: {
            // entry for the page
            entry: 'src/pages/PricingV1/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Pricing - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'pricing']
        },
        termcondition: {
            // entry for the page
            entry: 'src/pages/TermCondition/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Term Condition - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'termcondition']
        },
        blog_detail: {
            // entry for the page
            entry: 'src/pages/BlogDetail/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Blog - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'blog_detail']
        },
        login: {
            // entry for the page
            entry: 'src/pages/Login/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Login - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'login']
        },
        google: {
            // entry for the page
            entry: 'src/pages/GoogleLogin/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Logging In',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'google']
        },
        register: {
            // entry for the page
            entry: 'src/pages/Register/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Register - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'register']
        },
        payment: {
            // entry for the page
            entry: 'src/pages/Payment/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Payment - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'payment']
        },
        payment_result: {
            // entry for the page
            entry: 'src/pages/PaymentResult/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Payment - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'payment_result']
        },
        dashboard: {
            // entry for the page
            entry: 'src/pages/FreeTrial/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Dashboard Trial - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'dashboard']
        },
        registered: {
            // entry for the page
            entry: 'src/pages/Registered/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Registered - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'registered']
        },
        activation: {
            // entry for the page
            entry: 'src/pages/Activation/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Verifikasi',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'activation']
        },
        'masking-wa': {
            // entry for the page
            entry: 'src/pages/WABlastForm/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Whatsapp Blast - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'masking-wa']
        },
        wasuccessed: {
            // entry for the page
            entry: 'src/pages/WAFormFinished/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Whatsapp Blast - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'wasuccessed']
        },
        plan: {
            // entry for the page
            entry: 'src/pages/PaymentForm/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Plan - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'plan']
        },
        order_history: {
            // entry for the page
            entry: 'src/pages/OrderHistory/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Order History - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'order_history']
        },
        order_detail: {
            // entry for the page
            entry: 'src/pages/OrderDetail/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Detail Order - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'order_detail']
        },
        invoice: {
            // entry for the page
            entry: 'src/pages/InvoiceDownload/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Invoice - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'invoice']
        },
        pricing_umkm: {
            // entry for the page
            entry: 'src/pages/PricingUmkm/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Pricing UMKM - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'pricing_umkm']
        },
        login_umkm: {
            // entry for the page
            entry: 'src/pages/LoginUmkm/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Pricing UMKM - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'login_umkm']
        },
        verification_umkm: {
            // entry for the page
            entry: 'src/pages/VerificationUmkm/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Verification UMKM - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'verification_umkm']
        },
        thankyou: {
            // entry for the page
            entry: 'src/pages/Thankyou/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Thankyou - Telkom OCA',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', 'thankyou']
        },
        404: {
            // entry for the page
            entry: 'src/pages/404/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.html
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            filename: "404.html",
            title: "404",
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', '404']
        },
        500: {
            // entry for the page
            entry: 'src/pages/500/main.js',
            // the source template
            template: 'public/index.html',
            // output as dist/index.htmly
            // when using title option,
            // template title tag needs to be <title><%= htmlWebpackPlugin.options.title %></title>
            title: 'Error - Telkom OCA solusi omnichannel - Whatsapp bisnis broadcast, Email blast, ivr call dan sms blast',
            // chunks to include on this page, by default includes
            // extracted common chunks and vendor chunks.
            chunks: ['chunk-vendors', 'chunk-common', '500']
        },
    },
    chainWebpack: (config) => {
        config.mode('production');
        const svgRule = config.module.rule('svg');
        const SitemapPlugin = require('sitemap-webpack-plugin').default;
        const RobotstxtPlugin = require("robotstxt-webpack-plugin");
        
        const paths = ['/','*', '/*' ,'/500','/404','', '/umkm', '/pricing_umkm', '/products', '/pricing', '/blog?page=1', '/blog?page=2', '/blog?page=3', 'blog?page=4', '/blog?page=5', '/blog?page=1', '/blog?page=6', '/blog?page=7', '/blog?page=8', '/dashboard', '/register', '/login',
        "/blog_detail?blog=Langkah-Digitalisasi-Perusahaan-di-Era-Digital-dengan-Sistem-Informasi-yang-Efektif-dan-Efisien-untuk-Divisi-Human-Resources",
        "/blog_detail?blog=Inalum-MIND-ID-Spread-The-Name-Through-Social-Media",
        "/blog_detail?blog=Art-of-mass-communication-spread-the-information",
        "/blog_detail?blog=IVR-Call-dan-Manfaatnya",
        "/blog_detail?blog=Omnichannel-dalam-Era-E-Commerce-40",
        "/blog_detail?blog=Telkom-Indonesia-Meningkatkan-Customer-Experience-Melalui-OmniChannel",
        "/blog_detail?blog=WhatsApp-Broadcast-sebuah-channel-strategis-untuk-bisnis-Anda",
        "/blog_detail?blog=CPaaS-dan-UCaaS-Apa-pengaruhnya-untuk-bisnis-Anda",
        "/blog_detail?blog=WhatsApp-Business-akun-WhatsApp-untuk-anda-pebisnis",
        "/blog_detail?blog=6-Strategi-Promosi-bisnis-efektif-menghadapi-NEW-Normal",
        "/blog_detail?blog=Solusi-Omnichannel-untuk-Bisnis-Ritel",
        "/blog_detail?blog=Chatbot-Robot-Virtual-Untuk-Membantu-Bisnis-Anda",
        "/blog_detail?blog=8-Cara-Jitu-Mengembangkan-UMKM",
        "/blog_detail?blog=Promosi-Efektif-UMKM-Dengan-Email-Blast",
        "/blog_detail?blog=Strategi-Efektif-Mendapat-List-Email-Konsumen",
        "/blog_detail?blog=Strategi-Online-Marketing-Untuk-UMKM",
        "/blog_detail?blog=8-Jenis-Email-Marketing-Yang-Harus-Anda-Tahu",
        "/blog_detail?blog=UMKM-Harus-Go-Online",
        "/blog_detail?blog=6-Strategi-Efektif-UMKM",
        "/blog_detail?blog=Tips-Jitu-Mencari-Customer-Baru",
        "/blog_detail?blog=Membangun-Engagement-Dengan-Email-Blast-Marketing",
        "/blog_detail?blog=Tips-CTR-Tinggi-Pada-Email-Marketing",
        "/blog_detail?blog=Pentingnya-Database-Pelanggan-Untuk-Bisnis",
        "/blog_detail?blog=Pentingnya-Peran-Customer-Service-Untuk-Bisnis",
        "/blog_detail?blog=Cara-Meningkatkan-Kualitas-Layanan-Bisnis-Untuk-UMKM",
        "/blog_detail?blog=Memaksimalkan-Social-Commerce-Menggunakan-WhatsApp-Business",
        "/blog_detail?blog=Cara-Promosi-Efektif-Dengan-Budget-Terbatas",
        "/blog_detail?blog=Penyebab-Email-Marketing-Anda-Gagal",
        "/blog_detail?blog=Strategi-Digital-Marketing-Yang-Dapat-Diterapkan-Untuk-UKM",
        "/blog_detail?blog=Email-Blast-Untuk-Meningkatkan-Penjualan",
        "/blog_detail?blog=5-Platform-Email-Blast-Gratis"
        ];

        // new RobotstxtPlugin({
        //     filePath: 'robots.txt'
        // })
        config.plugin('sitemap')
            .use(SitemapPlugin, [{
                base: 'https://ocaindonesia.co.id/',
                paths,
                options: {
                    filename: 'sitemap.xml'
                }
            }]);

        config.plugin('robottxt')
            .use(RobotstxtPlugin, [{
                filePath: 'robots.txt'
            }]);
        svgRule.uses.clear();

        svgRule
            .use('babel-loader')
            .loader('babel-loader')
            .end()
            .use('vue-svg-loader')
            .loader('vue-svg-loader');
    },
}