# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:10
WORKDIR /app
COPY . .
RUN npm install --verbose
RUN ls -l
RUN npm run build
#Copy html webmaster google into dist folder
COPY ./googlee80ac1cf2150afab.html ./dist/

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx
COPY --from=0 /app/dist/ /usr/share/nginx/html
COPY ./conf/nginx.conf /etc/nginx/conf.d/default.conf
