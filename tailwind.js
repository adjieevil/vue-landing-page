module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.js',
  ],
  theme: {
    extend: {
      colors:{
        'primary': '#E81255',
        'primary-2': '#FF276B',
        'primary-hover': '#BF003C',
        'primary-hover-2': '#FFE9F0',
        'secondary': '#312F30',
        'secondary-hover': '#5A5959',
        'secondary-hover-2': '#E9E9E9',
        'complementer': '#F6F6FF',
        'complementer-2': '#B2B2C2',
        'complementer-3': '#737385',
        'complementer-4': '#4D4D5C',
        'complementer-5': '#3B3B47',
        'complementer-6': '#606070',
        'complementer-hover': '#9C9CAD',
        'complementer-hover-2': '#DFDFEB',
        'neutral-10': '#FAFBFC',
        'neutral-30': '#EBECF0',
        'neutral-40': '#DFE1E6',
        'neutral-70': '#B3BAC5',
        'neutral-80': '#A5ADBA',
        'neutral-100': '#7A869A',
        'neutral-200': '#6B778C',
        'neutral-300': '#5E6C84',
        'neutral-400': '#42526E',
        'neutral-500': '#253858',
        'neutral-600': '#091E42',
        'yellow-10': '#FFAB00',
        'customs-1': '#383637',
        'customs-2': '#3C415E',
        'customs-3': '#3D4145',
        'customs-4': '#383637',
        'customs-5': '#F7FAFB',
        'customs-6': '#FDCD85',
        'customs-7': '#A0DACA',
        'customs-8': '#D49DFF',
        'customs-9': '#82CAFF',
        'customs-10': '#F9F9F9',
        'customs-11': '#383C42',
        'customs-12': '#83888E',
        'customs-13': '#3584DD',
        'customs-14': '#737980',
        'customs-15': '#F4F5F7',
        'customs-16': '#20203F',
        'customs-17': '#2C2A2A',
        'customs-18': '#505052',

        //dtp lama
        'main-color': '#ED1260',
        'custom-1': '#36383A',
        'custom-2': '#312F30',
        'custom-3': '#C4C4C4',
        'custom-4': '#505052',
        'custom-5': 'rgba(49,47,48,0.6)', 
        'custom-6': 'rgba(54,56,58,0.8)',
        'custom-7': 'rgba(54,56,58,0.4)',
        'custom-8': '#C1C6CD',
        'custom-9': '#D3D3E0',
        'custom-10': '#8ADED4',
        'custom-11': '#48CBBC',
        'custom-12': '#E4EDF4',
        'custom-13': 'rgba(0,0,0,0.25)',
        'custom-14': '#A5BDE3',
        'custom-15': '#F99C5D',
        'custom-16': '#F6F6F9',
        'custom-17': '#22B9FF',
        'custom-18': '#FFB822',
        'custom-19': '#EB56DE',
        'custom-20': 'rgba(237,18,96,0.05)',
        'custom-21': '#25C2AF',
        'custom-22': '#7DE3D8',
        'custom-23': '#FFB680',
        'custom-24': '#3D2C42',
        'custom-25': '#C0C0C0',
        'custom-26': '#454445',
        'custom-27': '#E9E9E9',
        'custom-28': '#838383',
        'custom-29': '#001D43',
        'custom-30': '#FF276B',
        'custom-31': '#83888E',
        'custom-32': '#3584DD',
        'custom-33': '#E5E5E5;',
        'custom-34': '#F1F8FA;',
        'custom-35': '#737980;',
        'custom-36': '#FF75A0;',
        'custom-37': '#DDE1E6;',
        'custom-38': '#20203F;',
        'custom-39': '#2C2A2A;',
        'custom-40': '#FFD3D3;',
        'custom-41': '#FFFFFF;',
        'custom-42': '#4D5054;',

      },
      maxWidth: {
        'xss': '22rem',
      },
      spacing: {
        '2|3': '0.6rem',
        14: '3.5rem',
        68: '17rem',
        70: '18rem',
        74: '20rem',
        78: '22rem',
        '396px': '396px',
        '296px': '296px',
        '326px': '326px',
        '45%': '45%',
        730: '730px',
        820: '820px',
        900: '900px',
        1000: '1000px',
        1100: '1100px',
      },
      fontSize: {
        'xsm': '.8rem',
        'xxsm': '.9rem',
        // 'sm': '1.5rem',
        '3/4xl': '2rem',
        '4/5xl': '2.5rem',
        'size-1': '262.12px',
        'size-2': '192.12px',
        'size-3': '162.12px',
      },
      padding:{
        'p-60': '60px'
      },
      borderRadius: {
        xlg: '1rem',
        xl: '2rem'
      },
      inset: {
        '-5': '-5%',
        '-10': '-10%',
        '-15': '-15%',
        '-20': '-20%',
        '-25': '-25%',
        '-30': '-30%',
        '-50': '-50%',
        2: '2%',
        3: '3%',
        5: '5%',
        6: '6%',
        7: '7%',
        8: '8%',
        10: '10%',
        15: '15%',
        17: '17%',
        18: '18%',
        19: '19%',
        20: '20%',
        22: '22%',
        25: '25%',
        28: '28%',
        30: '30%',
        35: '35%',
        40: '40%',
        45: '45%',
        50: '50%',
        60: '60%',
        70: '70%',
      },
      height:{
        'h-price': '500px'
      },
      lineHeight:{
        '12': '3.25rem',
        'custom-1': '3rem'
      },
      zIndex: {
        '-10': '-10',
      },
      screens: {
        'xxl': {'min-width': '1600px'},
        'xxxl': {'min-width': '1873px'},
        'xls': {'min-width': '1360px', 'max-width': '1489px'},
        'slx': {'min-width': '1490px', 'max-width': '1599px'},
        'sml': {'max': '479px'},
        'smx': {'max': '767px'},
        // => @media (orientation: portrait) { ... }
      },
      letterSpacing: {
        '.3rem': '.3rem'
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
      borderColor: ['active'],
    }
  },
  plugins: [],
}
